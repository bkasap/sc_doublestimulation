from sys import stdout
import pandas as p
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.colors as mcolors

def distance(neuronposx, neuronposy, u, v):
    return np.sqrt((neuronposx-u)**2+(neuronposy-v)**2)

def getspiketrain(spikes, neuronid):
    train = []
    for i in spikes:
        if i[1]==neuronid:
            train.append(i[0])
    if not train:
        #print "no spikes!"
        return np.array([0]);
    else:
        return np.array(train)

def frate(st, window_width=5, dt=0.01):
    '''
    st : spike train
    window_width : gaussian kernel width in ms
    dt : time step in ms
    '''
    width_dt = int(window_width/dt)
    window = exp(-arange(-4 * width_dt, 4 * width_dt + 1) ** 2 * 1. / (2 * (width_dt) ** 2))
    rate = zeros(int(1000./(dt)))
    if len(st)>0:
        st = [int(x/dt) for x in st]
        rate[st] = 1000./dt
    return convolve(rate, window * 1. / sum(window), mode='same')

def Fpeak(R):
    F0 = 800
    beta = 00.07
    return F0/np.sqrt(1+beta*R)

def colorpic1(val):
    cmap = plt.get_cmap('Blues')
    return cmap(int(val*256))

def colorpic2(val):
    cmap = plt.get_cmap('PRGn')
    return cmap(int(val*256))

def colorpic3(val):
    cmap = plt.get_cmap('viridis')
    return cmap(int(val*256))

def eyetrace(filenamebase):
    neuron = p.read_csv(filenamebase+"neuroninfo.csv", ' ')
    spikes = p.read_csv(filenamebase+"spiketimes.csv", ' ')
    neuron = neuron.values
    spikes = spikes.values

    ccid = neuron[0,0]
    stcent = getspiketrain(spikes, ccid)
    start = stcent[0]
    
    time =  []
    hor = []
    ver = []
    for spike in spikes:
        time.append(spike[0])
        hor.append(exp(neuron[spike[1]][1])*cos(neuron[spike[1]][2]))
        ver.append(exp(neuron[spike[1]][1])*sin(neuron[spike[1]][2]))

    time.append(spike[0])
    hor.append(hor[-1])
    ver.append(ver[-1])
    return time, hor, ver

def simpleaxis(ax):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

