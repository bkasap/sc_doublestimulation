doublestimulation: doublestimulation.o

doublestimulation.o: doublestimulation.cu
	nvcc -G -g -O0 -gencode arch=compute_35,code=sm_35  -odir "." -M -o "$(@:%.o=%.d)" "$<"
	nvcc -G -g -O0 --compile --relocatable-device-code=true -gencode arch=compute_35,code=compute_35 -gencode arch=compute_35,code=sm_35  -x cu -o  "$@" "$<"

doublestimulation: doublestimulation.o
	nvcc --cudart static --relocatable-device-code=true -gencode arch=compute_35,code=compute_35 -gencode arch=compute_35,code=sm_35 -link -o  doublestimulation  doublestimulation.o

all: doublestimulation

clean:
	rm *.o *.d doublestimulation

rmdata:
	rm -rf run