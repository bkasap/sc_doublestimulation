#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <sstream>

using namespace std;

#define sugarman 0

#define C		600.0f			// Membrane capacitance (pF)
#define gL		20.0f			// Leak conductance (nS)
#define El		-53.0f			// Leak reversal potential (mV)
#define DeltaT	2.0f			// Spike slope factor (mV)
#define VT		-50.0f			// Exponential threshold (mV)
#define Vpeak	-30.0f			// Spike threshold (mV)
#define Vrst	-45.0f			// Reset potential (15mV)
#define a		0.0f			// Sub-threshold adaptation (nS)
#define	b		120.0f			// Spike-triggered adaptation (pA)
#define h		0.01f			// Time step (ms)

#define Eexc	0.0f			// Excitatory Reversal Potential (mV)
#define Einh	-80.0f			// Inhibitory Reversal Potential (mV)
#define	tauexc	5.0f			// Excitatory Conductance Decay (ms)
#define tauinh	10.0f			// Inhibitory Conductance Decay (ms)

// Define neuron class with all variables and parameters
class Neuron
{
	public:
		float	tauw;
		float 	V;
		float	w;
		float	g_e;
		float	g_i;
		float	I;
		float	posx;
		float	posy;
		int		nospks;
		int		neuronid;

};

// Membrane potential update
__device__ float dVdt(float V, float w, float ge, float gi, float I){
	return (-gL*(V-El)+gL*DeltaT*__expf((V-VT)/DeltaT)-w+ge*(Eexc-V)+gi*(Einh-V)+I)/C;
}

// Adaptive current update
__device__ float dwdt(float V, float w, float tauw){
	return (a*(V-El)-w)/tauw;
}

// Synaptic conductance
__device__ float dgdt(float g, float tau){
	return -g/tau;
}

// Initializes a vector with provided values on GPU
__global__ void initVector(int N, float *vec, float val){
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if ( i<N )
		vec[i]=val;
}

// Initializes neural parameters
__global__ void initNeuron(int N, Neuron *neuron, float Vrest){
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if ( i<N ){
		neuron[i].V = Vrest;
		neuron[i].w = 0.0f;
		neuron[i].g_e = 0.0f;
		neuron[i].g_i = 0.0f;
		neuron[i].I = 0.0f;
		neuron[i].posx = (i%201)*(5.0f/200);
		neuron[i].posy = (i/201)*(5.0f/200)-2.5f;
		neuron[i].nospks = 0;
		neuron[i].tauw = 100-abs(neuron[i].posx)*14.0f;
		neuron[i].neuronid = i;
	}
}

// Returns squared distance between 2 Neurons
__device__ float distance2(Neuron neuron1, Neuron neuron2){
	float dist2 = pow(neuron1.posx-neuron2.posx, 2)+pow(neuron1.posy-neuron2.posy, 2);
	return dist2;
}

// Calculates Gaussian synaptic weights
__device__ float synweight(float dist2, float sigma, float amp){
	float gauss = amp*__expf(-dist2/(2*sigma*sigma));
	return gauss;
}

// Location dependent synaptic weighting, tauw-s curve
__device__ float weight(float tauw){
	return 0.01*(8.808e-09*powf(tauw, 5) - 3.28e-06*powf(tauw, 4) + 0.0004855*powf(tauw,3) - 0.03607*powf(tauw,2) + 1.383*tauw - 8.396);
}

// Spike propagation
__global__ void propagateSpikes(int N, Neuron spiked, Neuron* neurons, float excamp, float inhamp){

	int i = blockIdx.x*blockDim.x + threadIdx.x;
	//if ( i == 20075 || i == 20023 || i == 19842 )
	//	printf("########################## SPIKE is %f away from cell %d \n", dist2, i);
	if (i<N){
		atomicAdd(&neurons[i].g_e, weight(neurons[i].tauw)* synweight(distance2(neurons[i], spiked), 0.4f, excamp));
		atomicAdd(&neurons[i].g_i, weight(neurons[i].tauw)* synweight(distance2(neurons[i], spiked), 1.2f, inhamp));
		//atomicAdd(&neurons[i].g_e, synweight(distance2(neurons[i], spiked), 0.3f, excamp));
		//atomicAdd(&neurons[i].g_i, synweight(distance2(neurons[i], spiked), 0.9f, inhamp));
	}
}

// Flush microstimulation values, initialize neurons with I=0
__global__ void killstimulation(int N, Neuron* neurons){
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if ( i<N )
		neurons[i].I = 0.0f;
}

// Microstimulation (exponential decrease with distance from center)
__global__ void microstimulation(int N, float amp, Neuron* neurons, Neuron position){
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if ( i<N )
		neurons[i].I += amp*__expf(-sqrt(distance2(neurons[i], position))*10);
}

// Double stimulation (microstimulation function at two given locations
__global__ void doublestimulation(int N, float amp, float amp2, Neuron* neurons, Neuron position1, Neuron position2){

	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if ( i<N )
		neurons[i].I += amp*__expf(-sqrt(distance2(neurons[i], position1))*10)+ amp2*__expf(-sqrt(distance2(neurons[i], position2))*10);
}

// Afferent mapping from visual space to collicular surface
__host__ Neuron affmapping(float R, float phi){
	Neuron target;
	target.posx = log(R);					// amplitude (degree)
	target.posy = phi/180*3.141593;			// direction (degree to radians)
	return target;
}

// Write filename
const char * filename(char * buffer, string varname){
	string fname;
	fname = buffer + varname;
	return fname.c_str();
}

// Unit step function
__device__ float unitstepfunc(float t){
	float I;
	if ( t>50 and t<150 )
		I = 1;
	else
		I = 0;
	return I;
}

// Update state of the simulation (timestep kernel)
__global__ void stateUpdate(int N, float t, int tidx,
							Neuron *neuron,
							Neuron *nextneuron,
							bool *spike,
							bool lateral,
							float excamp,
							float inhamp){
	int i = blockIdx.x*blockDim.x + threadIdx.x;

	// Sugarman for debugging
	if (sugarman == 1){
		neuron[i].I = unitstepfunc(t)*100;
	}

	if ( i < N ){//

		spike[i] = false;

		nextneuron[i]=neuron[i];

		// Runge-Kutta method to update state variables
		float v1, v2, v3, v4;
		float w1, w2, w3, w4;
		float ge1, ge2, ge3, ge4;
		float gi1, gi2, gi3, gi4;

		v1 = h*dVdt(neuron[i].V, neuron[i].w, neuron[i].g_e, neuron[i].g_i, neuron[i].I);
		w1 = h*dwdt(neuron[i].V, neuron[i].w, neuron[i].tauw);
		ge1 = h*dgdt(neuron[i].g_e, tauexc);
		gi1 = h*dgdt(neuron[i].g_i, tauinh);

		v2 = h*dVdt(neuron[i].V+(v1*0.5f), neuron[i].w+(w1*0.5f), neuron[i].g_e+(ge1*0.5f), neuron[i].g_i+(gi1*0.5f), neuron[i].I);
		w2 = h*dwdt(neuron[i].V+(v1*0.5f), neuron[i].w+(w1*0.5f), neuron[i].tauw);
		ge2 = h*dgdt(neuron[i].g_e+(ge1*0.5f), tauexc);
		gi2 = h*dgdt(neuron[i].g_i+(gi1*0.5f), tauinh);

		v3 = h*dVdt(neuron[i].V+(v2*0.5f), neuron[i].w+(w2*0.5f), neuron[i].g_e+(ge2*0.5f), neuron[i].g_i+(gi2*0.5f), neuron[i].I);
		w3 = h*dwdt(neuron[i].V+(v2*0.5f), neuron[i].w+(w2*0.5f), neuron[i].tauw);
		ge3 = h*dgdt(neuron[i].g_e+(ge2*0.5f), tauexc);
		gi3 = h*dgdt(neuron[i].g_i+(gi2*0.5f), tauinh);

		v4 = h*dVdt(neuron[i].V+v3, neuron[i].w+w3, neuron[i].g_e+ge3, neuron[i].g_i+gi3, neuron[i].I);
		w4 = h*dwdt(neuron[i].V+v3, neuron[i].w+w3, neuron[i].tauw);
		ge4 = h*dgdt(neuron[i].g_e+ge3, tauexc);
		gi4 = h*dgdt(neuron[i].g_i+gi3, tauinh);

		nextneuron[i].V = neuron[i].V + (v1 + 2.0f*v2 + 2.0f*v3 + v4)/6.0f;
		nextneuron[i].w = neuron[i].w + (w1 + 2.0f*w2 + 2.0f*w3 + w4)/6.0f;
		nextneuron[i].g_e = neuron[i].g_e + (ge1 + 2.0f*ge2 + 2.0f*ge3 + ge4)/6.0f;
		nextneuron[i].g_i = neuron[i].g_i + (gi1 + 2.0f*gi2 + 2.0f*gi3 + gi4)/6.0f;

		// Check for spikes
		if (nextneuron[i].V>Vpeak || !isfinite(nextneuron[i].V) ){
			// if there is a spike, propagate spikes
			printf("*");
			neuron[i].V = Vrst;						// Membrane potential reset
			neuron[i].w = neuron[i].w+b;			// Spike triggered adaptation
			neuron[i].nospks++;
			spike[i] = true;

			if ( lateral == true ){
				// Network simulations spike propagation (CUDA dynamic parallelism, compute capability 3.5)
				// requires separate compilation for CUDA (Generate PTX 3.5 Generate GPU 3.5)
				// (you can check your device compatibility with deviceQuery CUDA Sample)
				// sugarman tuning
				if (sugarman == 1)
					neuron[i].g_e = neuron[i].g_e + excamp;
				else
					propagateSpikes<<<256,256>>>(N, neuron[i], neuron, excamp, inhamp);
			}

		}
		else{
			// if no spikes, no updates
			neuron[i]=nextneuron[i];
		}
		cudaDeviceSynchronize();
	}
}

// write to file
void writeNeuronState(std::ofstream& output, Neuron neur){
	output << neur.V << " " << neur.w << " " << neur.g_e << " " << neur.g_i << " " << neur.I << " "<< neur.neuronid << "\n";
}

// write to file
void writeNeuronInfo(std::ofstream& output, Neuron neur){
	output << neur.neuronid << " " << neur.posx << " " << neur.posy << " " << neur.tauw << " " << neur.nospks << "\n";
}

// simulate a saccade with double stimulation
int simulatesaccade(float amp,		// stimulation location / saccade amplitude in degrees
					float dir,		// stimulation location / saccade direction in degrees
					float amp2,		// stimulation location 2 saccade amplitude in degrees
					float dir2,		// stimulation location 2 saccade direction in degrees
					int delay,		// delay between stimulation onsets
					float inpI,		// stimulation location / input current amplitude
					float inpI2,	// stimulation location 2 input current amplitude
					int mic,		// microstimulation protocol 0, 1, 2
					int dur,		// duration of microstimulation
					bool lateral,	// lateral connections on/off
					float excamp,	// excitatory connection coefficient
					float inhamp){	// inhibitory connection coefficient

	// membrane potential write to file
	bool details = false;

	// Initialize Neurons
	int N = 201*201;			//ccid variable depends on N
	Neuron *d_Neuronnext, *d_Neuron;
	cudaGetErrorString(cudaMalloc(&d_Neuron, N*sizeof(Neuron)));
	cudaGetErrorString(cudaMalloc(&d_Neuronnext, N*sizeof(Neuron)));

	Neuron *h_Neuron;
	h_Neuron = (Neuron *)malloc(N*sizeof(Neuron));

	initNeuron<<<256,256>>>(N, d_Neuron, El);
	initNeuron<<<256,256>>>(N, d_Neuronnext, El);

	// Initialize Spike Monitors
	bool *d_spikes;
	bool *h_spikes;
	h_spikes = (bool *)malloc(N*sizeof(bool));
	cudaGetErrorString(cudaMalloc(&d_spikes, N*sizeof(bool)));

	Neuron targetpos, targetpos2;
	targetpos = affmapping(amp, dir);
	targetpos2 = affmapping(amp2, dir2);

	// Hardcoding going on here!!!! pay attention to /0.025 calculation depends on number of neurons
	// closest cell id / central cell id
	int ccid = round(targetpos.posx/0.025)+round((targetpos.posy+2.5f)/0.025)*201;

	cudaGetErrorString(cudaMemcpy(h_Neuron, d_Neuron, N*sizeof(Neuron), cudaMemcpyDeviceToHost));

	printf("\nClosest cell id: %d for amp:%.0f and dir:%.0f\n", ccid, amp, dir);
	printf("## where targetpos.posx=%.3f and targetpos.posy=%.3f\n", targetpos.posx, targetpos.posy);
	printf("and neuron[%d].posx=%.3f and neuron[%d].posy=%.3f\n", ccid, h_Neuron[ccid].posx, ccid, h_Neuron[ccid].posy);

	// Open file streams to save simulation results
	char buffer [120];
	string fname;
	snprintf(buffer, sizeof(buffer), "run/lat_%d_inp_%.0f_inp2_%.0f_dur_%d_mic_%d_amp_%.0f_dir_%.0f_amp2_%.0f_dir2_%.0f_del_%d_e_%.4f_i_%.4f_", lateral, inpI, inpI2, dur, mic, amp, dir, amp2, dir2, delay, excamp, inhamp);

	ofstream neuronout1(filename(buffer, "neuron1.csv"));
	ofstream neuronout2(filename(buffer, "neuron2.csv"));
	ofstream neuronout3(filename(buffer, "neuron3.csv"));
	ofstream neuronout4(filename(buffer, "neuron4.csv"));
	ofstream neuronout5(filename(buffer, "neuron5.csv"));
	ofstream neuronout6(filename(buffer, "neuron6.csv"));
	ofstream neuronout7(filename(buffer, "neuron7.csv"));

	fname = buffer + string("gesnap.out");
	ofstream gesnapshot(fname.c_str());
	fname = buffer + string("gisnap.out");
	ofstream gisnapshot(fname.c_str());
	fname = buffer + string("microsnap.csv");
	ofstream microstim(fname.c_str());
	for (int i=0; i<201; i++)
		microstim << i << " ";
	microstim << "\n";

	fname = buffer + string("spiketimes.csv");
	ofstream spikes(fname.c_str());
	spikes << "SpkTime" << " " << "NeuronID" << "\n";

	cudaDeviceSynchronize();
	int counter = 0;
	int startstim = 50/h;
	int stopstim = startstim+dur/h;
	delay = delay/h;

	// Main simulation loop
	for ( float t=0.0f; t<350; t+=h ){
		counter++;

		if ( counter%10000==0 )
			printf("\nTime: %f", t);

		if ( mic == 1 ){
			// Single site stimulation (additive)
			// Microstimulation start and end
			if ( counter == startstim )
				microstimulation<<<256,256>>>(N, inpI, d_Neuron, targetpos);
			if ( counter == stopstim )
				killstimulation<<<256,256>>>(N, d_Neuron);
		} else if ( mic == 2 ){
			// Synchronous double stimulation start and end
			if ( counter == startstim )
				doublestimulation<<<256,256>>>(N, inpI, inpI2, d_Neuron, targetpos, targetpos2);
			if ( counter == stopstim )
				killstimulation<<<256,256>>>(N, d_Neuron);
		} else if ( mic == 3 ){
			// Double stimulation with delay in 4 steps (start1, start2, end1, end2)
			if ( counter == startstim )
				microstimulation<<<256,256>>>(N, inpI, d_Neuron, targetpos);
			if ( counter == startstim+delay ){
				killstimulation<<<256,256>>>(N, d_Neuron);
				cudaDeviceSynchronize();
				doublestimulation<<<256,256>>>(N, inpI, inpI2, d_Neuron, targetpos, targetpos2);
			}
			if ( counter == stopstim ){
				killstimulation<<<256,256>>>(N, d_Neuron);
				cudaDeviceSynchronize();
				microstimulation<<<256,256>>>(N, inpI2, d_Neuron, targetpos2);
			}
			if ( counter == stopstim+delay )
				killstimulation<<<256,256>>>(N, d_Neuron);
		}

		cudaDeviceSynchronize();

		// update state of the network
		stateUpdate<<<256,256>>>(N, t, counter, d_Neuron, d_Neuronnext, d_spikes, lateral, excamp, inhamp);

		cout << ".";
		cudaDeviceSynchronize();
		cudaGetErrorString(cudaMemcpy(h_Neuron, d_Neuron, N*sizeof(Neuron), cudaMemcpyDeviceToHost));
		cudaGetErrorString(cudaMemcpy(h_spikes, d_spikes, N*sizeof(bool), cudaMemcpyDeviceToHost));

		if ( details )
		{
			writeNeuronState(neuronout1, h_Neuron[ccid]);
			writeNeuronState(neuronout2, h_Neuron[ccid+3]);
			writeNeuronState(neuronout3, h_Neuron[ccid+6]);
			writeNeuronState(neuronout4, h_Neuron[ccid+9]);
			writeNeuronState(neuronout5, h_Neuron[ccid+12]);
			writeNeuronState(neuronout6, h_Neuron[ccid+15]);
			writeNeuronState(neuronout7, h_Neuron[ccid+18]);
		}

		for ( int i=0; i<N; i++){
			if ( h_spikes[i] == true ){
				spikes << t << " " << i << "\n";
			}
		}
		if ( counter == startstim+dur/h/2 ){
			for ( int i=0; i<N; i++ ){
				if (i%201 == 0 && i!=0){
					gesnapshot << "\n";
					gisnapshot << "\n";
					microstim << "\n" << i/201;
				}
				gesnapshot << h_Neuron[i].g_e*(Eexc-h_Neuron[i].V) << " ";
				gisnapshot << h_Neuron[i].g_i*(h_Neuron[i].V-Einh) << " ";
				microstim << h_Neuron[i].I << " ";
			}
		}
		if ( counter%201 == 0 )
			cout << "\n";
	}

	// end of main simulation loop
	// write results and close

	fname = buffer + string("neuroninfo.csv");
	ofstream neuroninfo(fname.c_str());
	neuroninfo << "id" << " " << "posx" << " " << "posy" << " " << "tauw" << " " << "nospks" << "\n";
	writeNeuronInfo(neuroninfo, h_Neuron[ccid]);
	for ( int i=0; i<N; i++ ){
		writeNeuronInfo(neuroninfo, h_Neuron[i]);
	}
	neuroninfo.close();

	neuronout1.close();
	neuronout2.close();
	neuronout3.close();
	neuronout4.close();
	neuronout5.close();
	neuronout6.close();
	neuronout7.close();

	gesnapshot.close();
	gisnapshot.close();
	microstim.close();

	spikes.close();

	printf("\nDEVICE RESET");
	cudaDeviceReset();
	cout << "\nEnd of application!";

	return 0;
}
void singleSiteStimulation()
{
	// w/wo lateral connections
	// sweeping different locations single location / microstimulation protocol 1
	int mic=1;

	float inpI = 150.0f;
	float inpI2 = 0.0f;
	int dur = 100;

	float amp[] = { 2, 10, 15, 21, 21, 35, 45};
	float dir[] = { 0,  0,  0,  0, 30,  0,  0};
	float amp2[]= { 0,  0,  0,  0,  0,  0,  0};
	float dir2[]= { 0,  0,  0,  0,  0,  0,  0};
	int delay = 0;

	for ( int i=0; i<7; ++i ){
		simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay, inpI, inpI2, mic, dur, true, 0.45f, 0.14f);
		simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay, inpI, inpI2, mic, dur, false, 0.0f, 0.0f);
	}
}

void doubleStimulation()
{
	// w/wo lateral connections
	// sweeping different locations double stimulation / microstimulation protocol 2
	int mic=2;

	float inpI = 150.0f;
	float inpI2 = 150.0f;
	int dur = 100;

	float amp[] = {  20,  20,  20,  20,  20,  20,  20,  20,  20 };
	float dir[] = {  45,  30,  15,  10,  20,  25,  35,  40,  50 };
	float amp2[]= {  20,  20,  20,  20,  20,  20,  20,  20,  20 };
	float dir2[]= { -45, -30, -15, -10, -20, -25, -35, -40, -50 };
	int delay = 0;

	for ( int i=0; i<9; ++i ){
		simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay, inpI, inpI2, mic, dur, true, 0.45f, 0.14f);
		simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay, inpI, inpI2, mic, dur, false, 0.0f, 0.0f);
	}
}

void doubleStimulationSynch()
{
	// w/wo lateral connections
	// sweeping different input currents double stimulation / microstimulation protocol 2
	int mic=2;

	float inpI = 120.0f;
	int dur = 100;

	float inpI2[] = { 100.0f, 110.0f, 120.0f, 130.0f, 140.0f, 150.0f, 160.0f, 170.0f, 180.0f, 190.0f, 200.0f };

	float amp[] = {  20 };
	float dir[] = {  15 };
	float amp2[]= {  20 };
	float dir2[]= { -15 };
	int delay = 0;

	for ( int j=0; j<11; ++j ){
		for ( int i=0; i<1; ++i ){
			simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay, inpI, inpI2[j], mic, dur, true, 0.45f, 0.14f);
			simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay, inpI, inpI2[j], mic, dur, false, 0.0f, 0.0f);
		}
	}
}

void doubleStimulationDelay()
{
	// w/wo lateral connections
	// sweeping input timing double stimulation / microstimulation protocol 3
	int mic=3;

	float inpI = 120.0f;
	float inpI2 = 150.0f;
	int dur = 100;

	float amp[] = {  20 };
	float dir[] = {  30 };
	float amp2[]= {  40 };
	float dir2[]= { -30 };
	int delay[] = { 2, 5, 10, 20, 25, 30, 40, 45, 50, 55, 60};

	for ( int j=0; j<15; ++j ){
		for ( int i=0; i<3; ++i ){
			simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay[j], inpI, inpI2, mic, dur, true, 0.45f, 0.14f);
			simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay[j], inpI, inpI2, mic, dur, false, 0.0f, 0.0f);
		}
	}
}

void doubleStimulationDelayInput()
{
	// w/wo lateral connections
	// sweeping input timing and input currents double stimulation / microstimulation protocol 3
	int mic=3;

	float inpI2 = 150.0f;
	int dur = 100;

	float amp[] = {  20,   5 };
	float dir[] = {  30,  45 };
	float amp2[]= {  40,  35 };
	float dir2[]= { -30, -45 };

	int delay[] = { 0, 2, 5, 10, 20, 50 };

	float inpI[] = { 80.0f, 90.0f, 100.0f, 110.0f, 120.0f, 130.0f, 140.0f, 150.0f, 160.0f, 170.0f, 180.0f, 190.0f, 200.0f };

	for ( int k=0; k<13; ++k ){
		for ( int j=0; j<6; ++j ){
			for ( int i=0; i<2; ++i ){
				simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay[j], inpI[k], inpI2, mic, dur, true, 0.45f, 0.14f);
				simulatesaccade(amp[i], dir[i], amp2[i], dir2[i], delay[j], inpI[k], inpI2, mic, dur, false, 0.0f, 0.0f);
			}
		}
	}
}


int main(int argc, char** argv)
{
	/* In case of calling from bash pipeline
	for(int i=0; i<argc; ++i){
		cout<< argv[i] << "\n";
	}

	simulatesaccade(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5], argv[6], argv[7], argv[8], argv[9], argv[10], argv[11]);

	*/

	// Uncomment to run protocol
	// make sure run/ folder exists to write output to

	// Single-site stimulation
	// singleSiteStimulation();

	// Synchronous double stimulation with the same input current properties
	//doubleStimulation();

	// Synchronous double stimulation with varying input current for the second target
	//doubleStimulationSynch();

	// Asynchronous double stimulation
	doubleStimulationDelayInput();

	return 0;

}
